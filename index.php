<?php
$access_token = "EAABz2ecA7aYBABXZAOAWsxAdoDoN75XcXYz7fhDlIEkDRWd84kMJddB7iFZBIdJZAsm9kONicTe2uIIW2jUiYrS6VtdOcI4NnEsXVcLItkrerVQO4hfBT4D5Gn2p7nG5MQ6bKKaF06N39WlWIHst6s8BZBBjlS2qHJ6RP21GkAZDZD";
$verify_token = "fb_token";
$hub_verify_token = null;
 
if(isset($_REQUEST['hub_challenge'])) {
    $challenge = $_REQUEST['hub_challenge'];
    $hub_verify_token = $_REQUEST['hub_verify_token'];
}
 
 
if ($hub_verify_token === $verify_token) {
    echo $challenge;
}


$input = json_decode(file_get_contents('php://input'), true);

$sender = $input['entry'][0]['messaging'][0]['sender']['id'];
$message = $input['entry'][0]['messaging'][0]['message']['text'];
$attachment = '';
/**
 * Some Basic rules to validate incoming messages
 */
if(preg_match('[jam|jam sekarang|waktu|jam berapa|jam berapa sekarang]', strtolower($message))) {

    // Make request to Time API
    ini_set('user_agent','Mozilla/4.0 (compatible; MSIE 6.0)');
    $result = file_get_contents("http://www.timeapi.org/utc/now?format=%25a%20%25b%20%25d%20%25I:%25M:%25S%20%25Y");
    if($result != '') {
        $message_to_reply = $result;
    }
}
elseif(preg_match('[ganteng|siapa ganteng|terganteng|ganteng banget|ganteng sekali]', strtolower($message))) {
	$message_to_reply = 'Siapa lagi kalau bukan Een';
}
elseif(preg_match('[produk bagus|gambar produk|produk]', strtolower($message))){
	$message_to_reply = 'Coba cek ini ya ';
	$attachment = 'http://img.sociolla.com/8725-large_default/jupier-04-tsukika-package.jpg';
}
else {
    $message_to_reply = 'Sori bro, bot masih belum banyak data. Coba dengan pertanyaan Jam berapa sekarang ? atau Siapa paling ganteng didunia';
}
//print $message_to_reply;

//API Url
$url = 'https://graph.facebook.com/v2.6/me/messages?access_token='.$access_token;


//Initiate cURL.
$ch = curl_init($url);

//The JSON data.
if($attachment != '')
{
	$jsonData = '{
	    "recipient":{
	        "id":"'.$sender.'"
	    },
	    "message":{
	    	"attachment":{
	    		"type":"image",
	    		"payload":{
	    			"url":"'.$attachment.'"
	    		}
	    	}
	    }
	}';
}
else
{
	$jsonData = '{
	    "recipient":{
	        "id":"'.$sender.'"
	    },
	    "message":{
	        "text":"'.$message_to_reply.'"
	    }
	}';
}

//Encode the array into JSON.
$jsonDataEncoded = $jsonData;

//Tell cURL that we want to send a POST request.
curl_setopt($ch, CURLOPT_POST, 1);

//Attach our encoded JSON string to the POST fields.
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

//Execute the request
if(!empty($input['entry'][0]['messaging'][0]['message'])){
    $result = curl_exec($ch);
}